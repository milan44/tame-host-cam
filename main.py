import os
import struct
import pygame.camera
import asyncio
import websockets
import xxhash

from dotenv import load_dotenv

load_dotenv()

token = os.getenv("RandomToken")

print("Initializing cameras...")

pygame.camera.init(backend=None)

cameras = pygame.camera.list_cameras()

print("Using '" + cameras[0] + "'...")

cam = pygame.camera.Camera(cameras[0], (1920, 1080))
cam.start()


def frame():
    return cam.get_raw()


def random():
    fr = frame()

    by = bytearray()

    x = 0
    while x < len(fr):
        part = fr[x:x + 64]

        if part[0] == 0:
            continue

        h = map_hash(part)

        by += struct.pack(">I", h)

        x += 64

    return by


def map_hash(by):
    x = xxhash.xxh32()

    x.update(by)

    return x.intdigest()


def map_range(s, i, o):
    (a1, a2), (b1, b2) = i, o
    return b1 + ((s - a1) * (b2 - b1) / (a2 - a1))


def get_host(websocket):
    if websocket.remote_address is None:
        return "unknown"
    else:
        host, port = websocket.remote_address
        return host


print("Starting socket...")


async def echo(websocket, path):
    print("[" + get_host(websocket) + "] connected")

    try:
        async for message in websocket:
            if message == "random_" + token:
                print("[" + get_host(websocket) + "] random request")
                await websocket.send(random())
            else:
                print("[" + get_host(websocket) + "] unauthorized")
                await websocket.close(code=1000, reason="unauthorized")
    except websockets.ConnectionClosed:
        print("[" + get_host(websocket) + "] disconnected")
    except websockets.WebSocketException as e:
        print("[" + get_host(websocket) + "] exception: " + e)


start_server = websockets.serve(echo, '192.168.2.183', 9000)
print("Socket started!")

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
